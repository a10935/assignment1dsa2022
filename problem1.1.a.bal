syntax = "proto3";



// The greeting service definition.
service Greeter {
 // Sends a greeting
 rpc sayHello (AdminRequest) returns # Description
 (AdminReply);
}

// The request message with the user's name.
message AdminRequest {
 string name = 1;
}

// The response message with the greetings
message AdminReply {
 string message = 1;
}